{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a href=\"https://qworld.net\" target=\"_blank\" align=\"left\"><img src=\"../qworld/images/header.jpg\"  align=\"left\"></a>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Measurement"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "_prepared by Abuzer Yakaryilmaz_\n",
    "\n",
    "_MindQuantum adaptated by Xu Zhou_\n",
    "<br><br>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can measure a quantum system, and then the system is observed in one of its states. This is the most basic type of measurement in quantum computing. (There are more generic measurement operators, but we will not mention about them here.)\n",
    "\n",
    "The probability of the system to be observed in a specified state is the square value of its amplitude.\n",
    "\n",
    "- If the amplitude of a state is zero, then this state cannot be observed.\n",
    "- If the amplitude of a state is nonzero, then this state can be observed.\n",
    "\n",
    "\n",
    "For example, if the system is in quantum state \n",
    "\n",
    "$$\n",
    "\\begin{pmatrix}-\\frac{\\sqrt{2}}{\\sqrt{3}}\\\\\\\\\\frac{1}{\\sqrt{3}}\\end{pmatrix},\n",
    "$$\n",
    "\n",
    "then, after a measurement, we can observe the system either in state $|0\\rangle$ with probability $\\frac{2}{3}$ or in state $|1\\rangle$ with probability $\\frac{1}{3}$.\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Collapsing\n",
    "\n",
    "After the measurement, the system collapses to the observed state, and so the system is no longer in a superposition. Thus, the information kept in a superposition is lost. \n",
    "- In the above example, when the system is observed in state $|0\\rangle$, then the new state becomes $\\begin{pmatrix}1\\\\\\\\0\\end{pmatrix}$. \n",
    "- If it is observed in state $|1\\rangle$, then the new state becomes $\\begin{pmatrix}0\\\\\\\\1\\end{pmatrix}$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## The second experiment of the quantum coin flipping"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Remember the experiment set-up."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<img src=\"./images/photon5.jpg\" width=\"65%\">"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In this experiment, after the first quantum coin-flipping, we make a measurement.\n",
    "\n",
    "If the measurement outcome is state $|0\\rangle$, then we apply a second `Hadamard`.\n",
    "\n",
    "First, we trace the experiment analytically."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<table width=\"100%\"><tr>\n",
    "<td width=\"400px\" style=\"background-color:white;text-align:center;vertical-align:middle;\" cellpadding=0>\n",
    "    <img src=\"./images/tracing-2nd-exp.png\">\n",
    "    <br><br>\n",
    "</td>\n",
    "<td width=\"*\" style=\"background-color:white;text-align:left;vertical-align:top;\">"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<b> The first Hadamard </b> \n",
    " \n",
    "We start in state $ |0\\rangle = \\begin{pmatrix}1\\\\\\\\0\\end{pmatrix} $. Then, we apply `Hadamard` operator:\n",
    "   \n",
    "$$\n",
    "\\begin{pmatrix}\\frac{1}{\\sqrt{2}}\\\\\\\\\\frac{1}{\\sqrt{2}}\\end{pmatrix} = \\frac{1}{\\sqrt{2}}\\begin{pmatrix}1&1\\\\\\\\1&-1\\end{pmatrix} \\begin{pmatrix}1\\\\\\\\0\\end{pmatrix}\n",
    "$$\n",
    "    \n",
    "<b> The first measurement </b>\n",
    "\n",
    "Due to the photon detector A, the photon cannot be in superposition, and so it forces the photon to be observed in state $|0\\rangle$ or state $|1\\rangle$. This is a measurement.\n",
    "\n",
    "Since the amplitudes are $\\frac{1}{\\sqrt{2}}$, we observe each state with equal probability.\n",
    "\n",
    "Thus, with probability $\\frac{1}{2}$, the new quantum state is $|0\\rangle = \\begin{pmatrix}1\\\\\\\\0\\end{pmatrix} $.\n",
    "\n",
    "And, with probability $\\frac{1}{2}$, the new quantum state is $|1\\rangle = \\begin{pmatrix}0\\\\\\\\1\\end{pmatrix} $.\n",
    "\n",
    "\n",
    "<b> The second Hadamard </b>\n",
    "\n",
    "If the photon is in state $|0\\rangle$, then another `Hadamard` operator is applied.\n",
    "\n",
    "In other words, with probability $ \\frac{1}{2} $, the computation continues and another `Hadamard` is applied:\n",
    "\n",
    "$$\n",
    "\\begin{pmatrix}\\frac{1}{\\sqrt{2}} \\\\\\\\ \\frac{1}{\\sqrt{2}}\\end{pmatrix} = \\frac{1}{\\sqrt{2}}\\begin{pmatrix}1&1 \\\\\\\\ 1&-1 \\end{pmatrix} \\begin{pmatrix}1 \\\\\\\\ 0 \\end{pmatrix} \n",
    "$$\n",
    "    \n",
    "<b> The second measurement </b>\n",
    "\n",
    "Due to the photon detectors B1 and B2, we make another measurement. \n",
    "\n",
    "Thus, we observe state $|0\\rangle$ with probability $ \\frac{1}{4} $ and state $|1\\rangle$ with probability $ \\frac{1}{4} $.\n",
    "\n",
    "At the end, the state $|0\\rangle$ can be observed with probability $ \\frac{1}{4} $, and the state $|1\\rangle$ can be observed with probability $ \\frac{3}{4} $."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<h3> Implementing the second experiment </h3>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We implement the second experiment in MindQuantum.\n",
    "\n",
    "For this purpose, MindQuantum provides a conditional operator based on the value of a classical register.\n",
    "\n",
    "In the following example, the last operator (`X`-gate) on the quantum register will be executed if the outcome of the measurement is 1.\n",
    "\n",
    "    sim = Simulator('projectq', 1)\n",
    "    …\n",
    "    outcome = sim.apply_gate(Measure().on(0))\n",
    "    \n",
    "    if outcome == 1:\n",
    "       sim.apply_gate(X.on(0))\n",
    "       \n",
    "Remark that we apply quantum operators through the simulator here. In this way, the conditional operators will be able to executed."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<h3> Task 1 </h3>\n",
    "\n",
    "Repeat the second experiment with the following modifications.\n",
    "\n",
    "Start in state $|1\\rangle$.\n",
    "\n",
    "Apply a `Hadamard` gate.\n",
    "\n",
    "Make a measurement. \n",
    "\n",
    "If the measurement outcome is 0, stop.\n",
    "\n",
    "Otherwise, apply a second `Hadamard`, and then make a measurement.\n",
    "\n",
    "Execute your circuit 1000 times.\n",
    "\n",
    "Calculate the expected values of observing '0' and '1', and then compare your result with the simulator result."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from mindquantum import *\n",
    "\n",
    "######################\n",
    "# Enter your code here\n",
    "######################"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a href=\"Q38_Measurement_Solutions.ipynb#task1\">click for our solution</a>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<h3> Task 2 </h3>\n",
    "\n",
    "Design the following quantum circuit.\n",
    "\n",
    "Start in state $|0\\rangle$.\n",
    "\n",
    "    Repeat 3 times:\n",
    "        make a measurement\n",
    "        if the qubit is in state |0>:\n",
    "            apply a Hadamard operator\n",
    "    make a measurement\n",
    "\n",
    "Execute your circuit 1000 times.\n",
    "\n",
    "Calculate the expected values of observing '0' and '1', and then compare your result with the simulator result."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from mindquantum import *\n",
    "\n",
    "######################\n",
    "# Enter your code here\n",
    "######################"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a href=\"Q38_Measurement_Solutions.ipynb#task2\">click for our solution</a>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<h3> Task 3 (extra) </h3>\n",
    "\n",
    "Design randomly created quantum circuit as follows.\n",
    "\n",
    "Pick four random binary values as $ r = [r0, r1, r2, r3]$.\n",
    "\n",
    "Start in state $|0\\rangle$.\n",
    "\n",
    "    apply a Hadamard operator\n",
    "    make a measurement\n",
    "    for j in range(4): \n",
    "        if the qubit in state |rj>:\n",
    "            apply a Hadamard operator\n",
    "        make a measurement\n",
    "        \n",
    "Print the random binary values, and guess the expected frequencies of observing '0' and '1' if the circuit is executed 1000 times.\n",
    "\n",
    "Then, execute your circuit 1000 times, and compare your guess with the simulator result.\n",
    "\n",
    "Repeat the task a few more times to verify your calculations."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "from random import randrange\n",
    "\n",
    "# We pick 4 random bits\n",
    "r = []\n",
    "for j in range(4):\n",
    "    r.append(randrange(2))\n",
    "\n",
    "print(r)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "_Calculate the frequencies on the paper or with using python_"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "######################\n",
    "# Enter your code here\n",
    "######################"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "_Execute the circuit 1000 times_"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Import all necessary objects and methods for quantum circuits\n",
    "import mindquantum as mq\n",
    "import numpy as np\n",
    "\n",
    "from mindquantum.simulator import Simulator\n",
    "from mindquantum.core.gates import H, X, Measure\n",
    "\n",
    "# Import randrange for random choices\n",
    "from random import randrange\n",
    "\n",
    "######################\n",
    "# Enter your code here\n",
    "######################"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a href=\"Q38_Measurement_Solutions.ipynb#task3\">click for our solution</a>"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.12"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
